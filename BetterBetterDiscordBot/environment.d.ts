declare global {
    namespace NodeJS {
        interface ProcessEnv {
            BOTTOKEN: string;
            GUILDID: string;
            USERID: string;
            SERVER_OWNER: string;
            ENVIRONMENT: "dev" | "prod" | "debug";
        }
    }
}

export {};