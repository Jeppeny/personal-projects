"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Bot = void 0;
const discord_js_1 = require("discord.js");
const glob_1 = require("glob");
const util_1 = require("util");
const rest_1 = require("@discordjs/rest");
const v9_1 = require("discord-api-types/v9");
const globPromise = (0, util_1.promisify)(glob_1.glob);
class Bot extends discord_js_1.Client {
    commands = new discord_js_1.Collection();
    constructor() {
        super({
            intents: [
                discord_js_1.Intents.FLAGS.GUILDS,
                discord_js_1.Intents.FLAGS.GUILD_MESSAGES,
                discord_js_1.Intents.FLAGS.GUILD_PRESENCES,
                discord_js_1.Intents.FLAGS.GUILD_MEMBERS,
                discord_js_1.Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
            ],
            partials: ["MESSAGE", "CHANNEL", "REACTION"],
        });
    }
    start() {
        this.registerModules();
        this.login(process.env.BOTTOKEN);
    }
    async importFile(filePath) {
        return (await Promise.resolve().then(() => __importStar(require(filePath))))?.default;
    }
    async registerCommands({ commands, guildId, clientId, }) {
        const rest = new rest_1.REST({
            version: "9",
        }).setToken(process.env.BOTTOKEN);
        (async () => {
            try {
                if (!guildId) {
                    await rest.put(v9_1.Routes.applicationCommands(clientId), {
                        body: commands,
                    });
                    console.log("Successfully registered application commands globally");
                }
                else {
                    await rest.put(v9_1.Routes.applicationGuildCommands(clientId, guildId), {
                        body: commands,
                    });
                    console.log(`Successfully registered application commands for guild ${guildId}`);
                }
            }
            catch (error) {
                console.error(error);
            }
        })();
        // if (guildId) {
        //   this.guilds.cache.get(guildId)?.commands.set(commands);
        //   console.log(`Registering commands to ${guildId}`);
        // } else {
        //   this.application?.commands.set(commands);
        //   console.log("Registering global commands");
        // }
    }
    async getCommandFilePaths() {
        const correctPath = __dirname.replace(/\\/g, "/");
        const commandFiles = await globPromise(`${correctPath}/../commands/*/*{.ts,.js}`);
        return commandFiles;
    }
    async getEventFilePaths() {
        const correctPath = __dirname.replace(/\\/g, "/");
        const eventFiles = await globPromise(`${correctPath}/../events/*{.ts,.js}`);
        return eventFiles;
    }
    async registerModules() {
        // Commands
        const slashCommands = [];
        this.getCommandFilePaths().then((commandFiles) => {
            commandFiles.forEach(async (filePath) => {
                const command = await this.importFile(filePath);
                if (!command.name)
                    return;
                this.commands.set(command.name, command);
                slashCommands.push(command);
            });
        });
        this.on("ready", () => {
            this.registerCommands({
                commands: slashCommands,
                guildId: process.env.guildId,
                clientId: process.env.USERID,
            });
        });
        // Events
        this.getEventFilePaths().then((eventFiles) => {
            eventFiles.forEach(async (filePath) => {
                const event = await this.importFile(filePath);
                this.on(event.event, event.run);
            });
        });
    }
}
exports.Bot = Bot;
