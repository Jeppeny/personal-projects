"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.client = void 0;
const client_1 = require("./structures/client");
require("dotenv").config();
exports.client = new client_1.Bot();
exports.client.start();
