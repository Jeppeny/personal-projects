"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const fs_1 = tslib_1.__importDefault(require("fs"));
const command_1 = require("../../structures/command");
exports.default = new command_1.Command({
    name: "scribbl",
    description: "Lisää sana scribbl io listaan",
    options: [
        {
            name: "sana",
            description: "Sana, jonka haluat lisätä listaan",
            required: true,
            type: 3 /* STRING */,
        },
    ],
    async run({ interaction, args, client }) {
        const tekija = interaction.user.username;
        const teksti = interaction.options.getString("sana");
        let scribbljson = fs_1.default.readFileSync("scribblio.json", "utf-8");
        let scribbl = JSON.parse(scribbljson);
        const ehotusLkm = scribbl.length;
        const ehotusArray = {
            "Sana Nro": ehotusLkm + 1,
            Tekijä: tekija,
            Sana: teksti,
        };
        scribbl.push(ehotusArray);
        scribbljson = JSON.stringify(scribbl, null, 4);
        fs_1.default.writeFileSync("scribblio.json", scribbljson + "\n", "utf-8");
        interaction.followUp("Sana lisätty listaan!");
    },
});
