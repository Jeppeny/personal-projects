"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const discord_js_1 = require("discord.js");
const fs_1 = tslib_1.__importDefault(require("fs"));
const command_1 = require("../../structures/command");
exports.default = new command_1.Command({
    name: "susamogus",
    description: "Näytä sanalista",
    async run({ interaction, args, client }) {
        let ehotuksetjson = fs_1.default.readFileSync("scribblio.json", "utf-8");
        let ehotukset = JSON.parse(ehotuksetjson);
        let tekstit = [];
        if (ehotukset.length == 0)
            return interaction.followUp("Ei sanoja! Käytä -scribbl ensin ja lisää sana!");
        for (const ehotus of ehotukset) {
            const t = ehotus["Sana"];
            tekstit.push(t);
        }
        const msg = tekstit.join(", ");
        const response = new discord_js_1.MessageEmbed()
            .setColor("DARK_AQUA")
            .setTitle("Tässä kaikki sanat scribblio-listasta:\n")
            .setDescription(msg);
        try {
            interaction.followUp({ embeds: [response], ephemeral: true });
        }
        catch (err) {
            interaction.followUp("Error");
            console.log(err);
        }
    },
});
