"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const discord_js_1 = require("discord.js");
const fs_1 = tslib_1.__importDefault(require("fs"));
const command_1 = require("../../structures/command");
exports.default = new command_1.Command({
    name: "scribbleio_add",
    description: "Lisää sana scribbl io listaan",
    options: [
        {
            name: "sana",
            description: "Sana(t), jonka haluat lisätä listaan (erotettuna pilkulla)",
            required: true,
            type: 3 /* STRING */,
        },
    ],
    async run({ interaction, args, client }) {
        const tekija = interaction.user.username;
        const teksti = interaction.options.getString("sana");
        let sanat = [];
        teksti.replace(/,\s*$/, "");
        if (teksti.includes(",")) {
            teksti.split(",").forEach(sana => {
                sana.trim();
                sanat.push(sana);
            });
        }
        else {
            sanat.push(teksti);
        }
        let scribbljson = fs_1.default.readFileSync("scribblio.json", "utf-8");
        let scribbl = JSON.parse(scribbljson);
        const ehotusLkm = scribbl.length;
        let sanatArray = [];
        for (let i = 0; i < ehotusLkm; i++) {
            sanatArray.push(scribbl[i]["Sana"]);
        }
        let msg = "Sanat, jotka olivat jo listassa: \n";
        sanat.forEach((sana) => {
            if (sanatArray.includes(sana))
                sanat.splice(sanat.indexOf(sana));
            console.log(`Sanaa ${sana} ei lisätty, sillä se oli jo listassa!`);
            msg += `❌${sana}\n`;
        });
        let i = ehotusLkm + 1;
        sanat.forEach((sana) => {
            const ehotusArray = {
                SanaNro: i,
                Tekijä: tekija,
                Sana: sana,
            };
            scribbl.push(ehotusArray);
            i++;
        });
        scribbljson = JSON.stringify(scribbl, null, 4);
        fs_1.default.writeFileSync("scribblio.json", scribbljson + "\n", "utf-8");
        const embed = new discord_js_1.MessageEmbed()
            .setColor("DARK_AQUA")
            .setTitle("Sana(t) lisätty listaan!💀\n\n")
            .setDescription(msg);
        interaction.followUp({ embeds: [embed] });
    },
});
