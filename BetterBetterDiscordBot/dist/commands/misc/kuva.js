"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Scraper = require("images-scraper");
const command_1 = require("../../structures/command");
const google = new Scraper({
    puppeteer: {
        headless: true,
    },
});
exports.default = new command_1.Command({
    name: "kuva",
    description: "Etsii kuvan googlesta",
    options: [
        {
            name: "searchterm",
            description: "What do you want an image of?",
            required: true,
            type: 3 /* STRING */,
        },
        {
            name: "random",
            description: "Use for randomizing image result",
            required: true,
            type: 5 /* BOOLEAN */,
        },
    ],
    run: async ({ client, interaction, args }) => {
        if (args.getBoolean("random")) {
            try {
                const haku = args.getString("search_term");
                const imageResults = await google.scrape(haku, 500);
                let kuvaNro = Math.floor(Math.random() * imageResults.length);
                return interaction.followUp(imageResults[kuvaNro].url);
            }
            catch (err) {
                console.log(err);
                return interaction.followUp("Kuvia ei löytynyt / haku epäonnistui");
            }
        }
        else if (!args.getBoolean("random")) {
            const haku = args.getString("search_term");
            try {
                const imageResults = await google.scrape(haku, 1);
                return interaction.followUp(imageResults[0].url);
            }
            catch (err) {
                console.log(err);
                return interaction.followUp("Kuvia ei löytynyt / haku epäonnistui");
            }
        }
    },
});
