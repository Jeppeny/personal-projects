"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const event_1 = require("../structures/event");
exports.default = new event_1.Event("ready", () => {
    console.log("AnoBotti is online!");
});
