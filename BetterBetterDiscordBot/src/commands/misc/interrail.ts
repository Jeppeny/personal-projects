import { ApplicationCommandOptionTypes } from "discord.js/typings/enums";
import { Command } from "../../structures/command";
import fs from "fs";
import { MessageEmbed } from "discord.js";
import { count } from "console";

export default new Command({
  name: "interrail",
  description: "Muokkaa/katsele interrail-suunnitelmaa",
  userRole: [],
  options: [
    {
      name: "valinta",
      description: "Mitä haluat tehdä",
      required: true,
      type: ApplicationCommandOptionTypes.STRING,
      choices: [
        { name: "näytä", value: "1" },
        { name: "muokkaa", value: "2" },
      ],
    },
  ],
  run: async ({ interaction, client, args }) => {
    const option = interaction.options.getString("valinta");
    let interrailJSON = fs.readFileSync("interrailPlan.json", "utf-8");
    let interrailPlan = JSON.parse(interrailJSON);

    function naytaInterrailSuunnitelma() {
      let embed = new MessageEmbed()
        .setColor("YELLOW")
        .setTitle("Interrail Suunnitelma");
      var suunnitelma: string = "";
      for (let i = 0; i < interrailPlan.length; i++) {
        let s: string = "";
        if (interrailPlan[i] != null) {
          s = `Päivä ${i + 1}:\nReitit: ${
            interrailPlan[i]["trips"]
          }\nKaupungit: ${interrailPlan[i]["cities"]}\nLisätiedot: ${
            interrailPlan[i]["lisaInfo"]
          }\n\n`;
        } else s = `Ei tietoja päivälle ${i + 1}\n\n`;

        suunnitelma += s;
      }
      embed.setDescription(suunnitelma);
      interaction.followUp({ embeds: [embed] });
    }

    async function muokkaaInterrailSuunnitelmaa() {
      const emojit = {
        "0️⃣": 0,
        "1️⃣": 1,
        "2️⃣": 2,
        "3️⃣": 3,
        "4️⃣": 4,
        "5️⃣": 5,
        "6️⃣": 6,
        "7️⃣": 7,
        "8️⃣": 8,
        "9️⃣": 9,
      };

      const emojiArray = [
        "0️⃣",
        "1️⃣",
        "2️⃣",
        "3️⃣",
        "4️⃣",
        "5️⃣",
        "6️⃣",
        "7️⃣",
        "8️⃣",
        "9️⃣",
      ];

      const sendEmbed = await interaction.channel.send({
        embeds: [
          new MessageEmbed()
            .setColor("YELLOW")
            .setTitle(
              "Mitä päivää haluat muokata? Jos päivää ei ole vielä lisätty, se lisätään suunnitelmaan."
            )
            .setDescription(
              "\n\nReagoi tähän viestiin sillä päivän numerolla (tai max. kahdella numerolla), jota haluat muokata!"
            ),
        ],
      });

      // Lisää tapa kysyä käyttäjältä päivä, reitit ja kaupungit
      // Tee interrail päivä interface

      const filter1 = (m) => {
        // check if NaN
        if (m.author.bot) return false;
        if (parseInt(m.content) > 0 && parseInt(m.content) <= 31) {
          return true;
        }
      };

      const botFilter = (m) => {
        if (m.author.bot) return false;
        else return true;
      };

      const reactionFilter = (reaction, user) => {
        if (emojiArray.includes(reaction.emoji.name) && !user.bot) {
          // Lisää oikean käyttäjän tarkistus
          return true;
        } else return false;
      };

      for (const emoji of emojiArray) sendEmbed.react(emoji);

      const reactionCollector1 = sendEmbed.createReactionCollector({
        filter: reactionFilter,
        time: 15000,
        max: 2,
      });

      reactionCollector1.on("end", (collected) => {
        let day: number = 0;
        console.log(collected);
        const reactionsMap = collected.map((reaction) => reaction.emoji.name);
        if (reactionsMap.length === 1) {
          const emoji = reactionsMap[0];
          day = emojit[emoji];
        }

        if (reactionsMap.length === 2) {
          const emoji1 = reactionsMap[0];
          const emoji2 = reactionsMap[1];
          day = parseInt(emojit[emoji1].toString() + emojit[emoji2].toString());
        }
        if (collected.map((message) => message.count)[0] === 2 && reactionsMap.length === 1) {
          const emoji1 = reactionsMap[0];
          day = parseInt(emojit[emoji1].toString() + emojit[emoji1].toString());
        }
        console.log(day);
        interaction.channel.send({
          embeds: [
            new MessageEmbed()
              .setColor("YELLOW")
              .setTitle(
                "Anna jokaiseen tietokenttään uusi tieto erillisessä viestissä tai '-' ohittaaksesi kentän"
              )
              .setDescription(
                "Kentät:\n1. Päivän reitit\n2. Kaupungit\n3. Lisäinfo\n\nLähetä kolme erillistä viestiä tässä järjestyksessä!"
              ),
          ],
        });
        const nro = day - 1; 
        let cities: string = "";
        let trips: string = "";
        let lisaInfo: string = "";
        if (!interrailPlan[nro]) {
          interaction.channel.send({
            embeds: [
              new MessageEmbed()
                .setColor("ORANGE")
                .setTitle("Päivää ei ollut vielä olemassa, uusi päivä luotu"),
            ],
          });
        } else {
          cities = interrailPlan[nro].cities;
          trips = interrailPlan[nro].trips;
          lisaInfo = interrailPlan[nro].lisaInfo;
          const paivanTiedotEmbed = new MessageEmbed()
            .setTitle("Päivän nykyiset tiedot:")
            .setDescription(
              `Kaupungit: ${interrailPlan[nro].cities}\nReitit: ${interrailPlan[nro].trips}\nLisätiedot: ${interrailPlan[nro].lisaInfo}`
            )
            .setColor("YELLOW");
          interaction.channel.send({ embeds: [paivanTiedotEmbed] });
        }

        const collector2 = interaction.channel.createMessageCollector({
          filter: botFilter,
          time: 240000,
          max: 3,
        });
        collector2.on("collect", (m) => {
          console.log("Message collected");
        });
        collector2.on("end", (collected) => {
          if (collected.size < 3)
            interaction.channel.send({
              embeds: [
                new MessageEmbed()
                  .setColor("RED")
                  .setTitle(
                    "Anna jokaiseen kenttään tekstiä tai '-', jos haluat jättää sen tyhjäksi! Yritä uudelleen!"
                  ),
              ],
            });
          else {
            const collectedMap = collected.map((message) => message.content);
            if (collectedMap[0] != "-") trips = collectedMap[0];
            if (collectedMap[1] != "-") cities = collectedMap[1];
            if (collectedMap[2] != "-") lisaInfo = collectedMap[2];
            interaction.channel.send({
              embeds: [
                new MessageEmbed()
                  .setColor("GREEN")
                  .setTitle("Päivän tiedot päivitetty onnistuneesti!"),
              ],
            });
          }
          const lisattavaPaiva = {
            cities: cities,
            trips: trips,
            lisaInfo: lisaInfo,
          };

          interrailPlan[nro] = lisattavaPaiva;

          interrailJSON = JSON.stringify(interrailPlan, null, 4);
          fs.writeFileSync("interrailPlan.json", interrailJSON + "\n", "utf-8");
        });
      });

      // const collector = interaction.channel.createMessageCollector({
      //   filter: filter1,
      //   time: 30000,
      //   max: 1,
      // });

      // collector.on("collect", (m) => {
      //   console.log(m.content);
      //   const uusiPaiva = new InterrailPaiva();
      //   interaction.followUp({
      //     embeds: [
      //       new MessageEmbed()
      //         .setColor("YELLOW")
      //         .setTitle(
      //           "Anna jokaiseen tietokenttään uusi tieto erillisessä viestissä tai '-' ohittaaksesi kentän"
      //         )
      //         .setDescription(
      //           "Kentät:\n1. Päivän reitit\n2. Kaupungit\n3. Lisäinfo\n\nLähetä kolme erillistä viestiä tässä järjestyksessä!"
      //         ),
      //     ],
      //   });
      //   const nro = parseInt(m.content) - 1;
      //   if (!interrailPlan[nro]) {
      //     uusiPaiva.changeCities("Tyhjä");
      //     uusiPaiva.changeDay(m.content);
      //     uusiPaiva.changeTrips("Tyhjä");
      //     uusiPaiva.changeInfo("Tyhjä");
      //     interaction.followUp({
      //       embeds: [
      //         new MessageEmbed()
      //           .setColor("ORANGE")
      //           .setTitle("Päivää ei ollut vielä olemassa, uusi päivä luotu"),
      //       ],
      //     });
      //   } else {
      //     uusiPaiva.changeCities(interrailPlan[nro].cities);
      //     uusiPaiva.changeDay(interrailPlan[nro]);
      //     uusiPaiva.changeTrips(interrailPlan[nro].trips);
      //     uusiPaiva.changeInfo(interrailPlan[nro].lisaInfo);
      //     const paivanTiedotEmbed = new MessageEmbed()
      //       .setTitle("Päivän nykyiset tiedot:")
      //       .setDescription(
      //         `Kaupungit: ${interrailPlan[nro].cities}\nReitit: ${interrailPlan[nro].trips}\nLisätiedot: ${interrailPlan[nro].lisaInfo}`
      //       )
      //       .setColor("YELLOW");
      //     interaction.followUp({ embeds: [paivanTiedotEmbed] });
      //   }

      //   const collector2 = interaction.channel.createMessageCollector({
      //     filter: botFilter,
      //     time: 240000,
      //     max: 3,
      //   });
      //   collector2.on("collect", (m) => {
      //     console.log("Message collected");
      //   });
      //   collector2.on("end", (collected) => {
      //     console.log(collected.size);
      //     if (collected.size < 3)
      //       interaction.followUp({
      //         embeds: [
      //           new MessageEmbed()
      //             .setColor("RED")
      //             .setTitle(
      //               "Anna jokaiseen kenttään tekstiä tai '-', jos haluat jättää sen tyhjäksi! Yritä uudelleen!"
      //             ),
      //         ],
      //       });
      //     else {
      //       const collectedMap = collected.map((message) => message.content);
      //       if (collectedMap[0] != "-") uusiPaiva.changeTrips(collectedMap[0]);
      //       if (collectedMap[1] != "-") uusiPaiva.changeCities(collectedMap[1]);
      //       if (collectedMap[2] != "-") uusiPaiva.changeInfo(collectedMap[2]);
      //       interaction.followUp({
      //         embeds: [
      //           new MessageEmbed()
      //             .setColor("GREEN")
      //             .setTitle("Päivän tiedot päivitetty onnistuneesti!"),
      //         ],
      //       });
      //     }
      //   });

      //   const lisattavaPaiva = {
      //     cities: uusiPaiva.cities,
      //     trips: uusiPaiva.trips,
      //     lisaInfo: uusiPaiva.lisaInfo,
      //   };

      //   interrailPlan[nro] = lisattavaPaiva;

      //   interrailJSON = JSON.stringify(interrailPlan, null, 4);
      //   fs.writeFileSync("interrailPlan.json", interrailJSON + "\n", "utf-8");
      // });

      reactionCollector1.on("end", (collected) => {
        if (!collected) {
          interaction.channel.send({
            embeds: [
              new MessageEmbed()
                .setColor("RED")
                .setTitle("Päivän numeroa ei annettu, yritä uudelleen!"),
            ],
          });
        }
      });
    }

    if (option === "1") {
      naytaInterrailSuunnitelma();
    }

    if (option === "2") {
      muokkaaInterrailSuunnitelmaa();
    }
  },
});
