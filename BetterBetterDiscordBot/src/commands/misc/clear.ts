// import { Message, MessageEmbed } from "discord.js";
// import { ApplicationCommandOptionTypes } from "discord.js/typings/enums";
// import { brotliCompress } from "zlib";
// import { Command } from "../../structures/command";

// export default new Command({
//   name: "clear",
//   description: "Clears messages",
//   options: [
//     {
//       name: "amount",
//       description: "How many messages do you want to delete? (max. 20)",
//       required: true,
//       type: ApplicationCommandOptionTypes.INTEGER,
//     },
//     {
//       name: "target",
//       description:
//         "Target user? (if not specified messages from everyone will be deleted)",
//       required: false,
//       type: ApplicationCommandOptionTypes.USER,
//     },
//   ],
//   async run({ client, interaction, args }) {
//     const amount = interaction.options.getInteger("amount");

//     if (amount > 20 && interaction.member.id != process.env.SERVER_OWNER)
//       return interaction.reply("Et voi poistaa yli 20 viestiä kerralla!");

//     if (amount >= 100)
//       return interaction.reply(
//         "KairaPro ei voi poistaa yli 100 viestiä kerralla t discord"
//       );

//     if (amount < 1)
//       return interaction.reply(
//         "Kerralla on poistettava vähintään yksi viesti!"
//       );

//     const messagesToDelete = amount + 1;
//     const getMessages = await interaction.channel.messages.fetch();
//     const targetUser = interaction.options.getUser("target");
//     const response = new MessageEmbed().setColor("DARK_AQUA");
    

//     console.log(getMessages);
//     if (targetUser) {
//       let i = 0;
//       const filtered: Message[] = [];

//       getMessages.filter(m => {
//             if (m.author.id === targetUser.id && amount > i) {
//                 filtered.push(m);
//                 i++;
//             }
//         });
//       return await interaction.channel.bulkDelete(filtered, true).then((messages) => {
//         response.setDescription(
//           `🧹 Cleared ${messages.size} messages from ${targetUser.username}.`
//         );
//         interaction.reply({embeds: [response]});
//       });
//     }

//     await interaction.channel.messages
//       .fetch({ limit: messagesToDelete })
//       .then((messages) => {
//         interaction.channel.bulkDelete(messages, true);
//       });
//   },
// });
