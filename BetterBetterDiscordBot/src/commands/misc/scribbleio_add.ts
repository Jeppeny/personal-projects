import { MessageEmbed } from "discord.js";
import { ApplicationCommandOptionTypes } from "discord.js/typings/enums";
import fs from "fs";
import { Command } from "../../structures/command";

export default new Command({
  name: "scribbleio_add",
  description: "Lisää sana scribbl io listaan",
  options: [
    {
      name: "sana",
      description: "Sana(t), jonka haluat lisätä listaan (erotettuna pilkulla)",
      required: true,
      type: ApplicationCommandOptionTypes.STRING,
    },
  ],
  run({ interaction, args, client }) {
    const tekija = interaction.user.username;
    var teksti = interaction.options.getString("sana");

    let sanat: string[] = [];
    teksti.trim()
    teksti = teksti.replace(/,\s*$/, "");
    if (teksti.includes(",")) {
      teksti.split(",").forEach((sana) => {
        sana.trim();
        sanat.push(sana);
      });
    } else {
      sanat.push(teksti);
    }
    let scribbljson = fs.readFileSync("scribblio.json", "utf-8");
    let scribbl = JSON.parse(scribbljson);
    const ehotusLkm = scribbl.length;
    let sanatArray: string[] = [];
    for (let i = 0; i < ehotusLkm; i++) {
      sanatArray.push(scribbl[i]["Sana"]);
    }
    let msg: string = "Sanat, jotka olivat jo listassa: \n";
    let samoja: boolean = false;
    sanat.forEach((sana) => {
      if (sanatArray.includes(sana)) {
        sanat.splice(sanat.indexOf(sana));
        console.log(`Sanaa ${sana} ei lisätty, sillä se oli jo listassa!`);
        msg += `❌${sana}\n`;
        samoja = true;
      }
    });
    let i = ehotusLkm + 1;
    sanat.forEach((sana) => {
      const ehotusArray = {
        SanaNro: i,
        Tekijä: tekija,
        Sana: sana.trim(),
      };
      scribbl.push(ehotusArray);
      i++;
    });

    scribbljson = JSON.stringify(scribbl, null, 4);
    fs.writeFileSync("scribblio.json", scribbljson + "\n", "utf-8");
    const embed = new MessageEmbed()
      .setColor("DARK_AQUA")
      .setTitle("Sana(t) lisätty listaan!💀\n\n");
    if (samoja) embed.setDescription(msg);
    interaction.followUp({ embeds: [embed] });
  },
});
