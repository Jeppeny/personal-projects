const Scraper = require("images-scraper");
import { Command } from "../../structures/command";
import { ApplicationCommandOptionTypes } from "discord.js/typings/enums";

const google = new Scraper({
  puppeteer: {
    headless: true,
  },
});

export default new Command({
  name: "kuva",
  description: "Etsii kuvan googlesta",
  options: [
    {
      name: "searchterm",
      description: "What do you want an image of?",
      required: true,
      type: ApplicationCommandOptionTypes.STRING,
    },
    {
      name: "random",
      description: "Use for randomizing image result",
      required: true,
      type: ApplicationCommandOptionTypes.BOOLEAN,
    },
  ],
  run: async ({ client, interaction, args }) => {
    if (args.getBoolean("random")) {
      try {
        const haku = args.getString("search_term");
        const imageResults = await google.scrape(haku, 500);
        let kuvaNro = Math.floor(Math.random() * imageResults.length);
        return interaction.followUp(imageResults[kuvaNro].url);
      } catch (err) {
        console.log(err);
        return interaction.followUp("Kuvia ei löytynyt / haku epäonnistui");
      }
    } else if (!args.getBoolean("random")) {
      const haku = args.getString("search_term");
      try {
        const imageResults = await google.scrape(haku, 1);
        return interaction.followUp(imageResults[0].url);
      } catch (err) {
        console.log(err);
        return interaction.followUp("Kuvia ei löytynyt / haku epäonnistui");
      }
    }
  },
});
