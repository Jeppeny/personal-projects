import { MessageEmbed } from "discord.js";
import fs from "fs";
import { Command } from "../../structures/command";

export default new Command({
  name: "susamogus",
  description: "Näytä sanalista",
  run({ interaction, args, client }) {
    let ehotuksetjson = fs.readFileSync("scribblio.json", "utf-8");
    let ehotukset = JSON.parse(ehotuksetjson);
    let tekstit = [];

    if (ehotukset.length == 0)
      return interaction.followUp(
        "Ei sanoja! Käytä -scribbl ensin ja lisää sana!"
      );

    for (const ehotus of ehotukset) {
      const t = ehotus["Sana"];
      tekstit.push(t);
    }
    const msg = tekstit.join(", ");
    const response = new MessageEmbed()
      .setColor("DARK_AQUA")
      .setTitle("Tässä kaikki sanat scribblio-listasta:\n")
      .setDescription(msg);

    try {
      interaction.followUp({ embeds: [response], ephemeral: true });
    } catch (err) {
      interaction.followUp("Error");
      console.log(err);
    }
  },
});
