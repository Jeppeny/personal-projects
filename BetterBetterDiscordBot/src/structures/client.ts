import {
  ApplicationCommandDataResolvable,
  Client,
  ClientEvents,
  Collection,
  Intents,
} from "discord.js";
import { CommandType } from "../typings/command";
import { glob } from "glob";
import { promisify } from "util";
import { RegisterCommandsOptions } from "../typings/client";
import { Event } from "./event";
import { REST } from "@discordjs/rest";
import { Routes } from "discord-api-types/v9";

const globPromise = promisify(glob);

export class Bot extends Client {
  commands: Collection<string, CommandType> = new Collection();

  constructor() {
    super({
      intents: [
        Intents.FLAGS.GUILDS,
        Intents.FLAGS.GUILD_MESSAGES,
        Intents.FLAGS.GUILD_PRESENCES,
        Intents.FLAGS.GUILD_MEMBERS,
        Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
      ],
      partials: ["MESSAGE", "CHANNEL", "REACTION"],
    });
  }

  start() {
    this.registerModules();
    this.login(process.env.BOTTOKEN);
  }

  async importFile(filePath: string) {
    return (await import(filePath))?.default;
  }

  async registerCommands({
    commands,
    guildId,
    clientId,
  }: RegisterCommandsOptions) {
    const rest = new REST({
      version: "9",
    }).setToken(process.env.BOTTOKEN);

    (async () => {
      try {
        if (!guildId) {
          await rest.put(Routes.applicationCommands(clientId), {
            body: commands,
          });
          console.log("Successfully registered application commands globally");
        } else {
          await rest.put(Routes.applicationGuildCommands(clientId, guildId), {
            body: commands,
          });
          console.log(
            `Successfully registered application commands for guild ${guildId}`
          );
        }
      } catch (error) {
        console.error(error);
      }
    })();

    // if (guildId) {
    //   this.guilds.cache.get(guildId)?.commands.set(commands);
    //   console.log(`Registering commands to ${guildId}`);
    // } else {
    //   this.application?.commands.set(commands);
    //   console.log("Registering global commands");
    // }
  }

  async getCommandFilePaths() {
    const correctPath = __dirname.replace(/\\/g, "/");
    const commandFiles = await globPromise(
      `${correctPath}/../commands/*/*{.ts,.js}`
    );
    return commandFiles;
  }

  async getEventFilePaths() {
    const correctPath = __dirname.replace(/\\/g, "/");
    const eventFiles = await globPromise(`${correctPath}/../events/*{.ts,.js}`);
    return eventFiles;
  }

  async registerModules() {
    // Commands
    const slashCommands: ApplicationCommandDataResolvable[] = [];

    this.getCommandFilePaths().then((commandFiles) => {
      commandFiles.forEach(async (filePath) => {
        const command: CommandType = await this.importFile(filePath);
        if (!command.name) return;
        this.commands.set(command.name, command);
        slashCommands.push(command);
      });
    });

    this.on("ready", () => {
      this.registerCommands({
        commands: slashCommands,
        guildId: process.env.guildId,
        clientId: process.env.USERID,
      });
    });

    // Events
    this.getEventFilePaths().then((eventFiles) => {
      eventFiles.forEach(async (filePath) => {
        const event: Event<keyof ClientEvents> = await this.importFile(
          filePath
        );
        this.on(event.event, event.run);
      });
    });
  }
}
