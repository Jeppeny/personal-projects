import { Bot } from "./structures/client";
require("dotenv").config();

export const client = new Bot();

client.start();
