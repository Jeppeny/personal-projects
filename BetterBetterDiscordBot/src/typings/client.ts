import { ApplicationCommandDataResolvable } from "discord.js";

export interface RegisterCommandsOptions {
  clientId?: string;
  guildId?: string;
  commands: ApplicationCommandDataResolvable[];
}
