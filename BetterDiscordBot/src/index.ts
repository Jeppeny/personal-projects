import Client from './Client';

const client = new Client()

async function main() {
    await client.init();
}

main().catch(console.error);