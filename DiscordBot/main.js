const Discord = require('discord.js');
const dotenv = require('dotenv');
const fs = require('fs');
const bot_config = require('./bot_config');

dotenv.config();

const client = new Discord.Client({
    intents: [
        Discord.Intents.FLAGS.GUILDS,
        Discord.Intents.FLAGS.GUILD_MESSAGES,
        Discord.Intents.FLAGS.GUILD_PRESENCES,
        Discord.Intents.FLAGS.GUILD_MEMBERS,
        Discord.Intents.FLAGS.GUILD_MESSAGE_REACTIONS
    ],
    partials: [
        "MESSAGE",
        "CHANNEL",
        "REACTION" 
    ]
});

client.commands = new Discord.Collection();
client.events = new Discord.Collection();

['command_handler', 'event_handler'].forEach(handler => {
    require(`./handlers/${handler}`)(client, Discord);
});

process.once('SIGINT', async () => {
    console.log('\nAnoBotti is going offline!');
    await client.channels.cache.get(bot_config.bot_channel).send('AnoBotti is going offline!');
    process.exit();
});

client.login(process.env.TOKEN);