const fs = require('fs');



module.exports = {
    name: 'poll',
    description: 'Tee kysely',
    async execute (client, message, args, Discord) {

        const channel = message.channel.id;
        let emojitjson = fs.readFileSync('./emojis.json', 'utf-8');
        let emojit = JSON.parse(emojitjson);


        var kysymys = [];
        var vaihtoEhdot = [];
        var valinnat = [];
        var reaktiot = [];
        var vastaukset = {};


        if (!args[0]) return message.reply('Käyttö: -poll (kirjoita tähän kysymys) !vaihtoehto1... ...!vaihtoehtoN emoji1... ...emojiN');

        for (var arg of args) {
            if (arg.startsWith('!')) {
                const vaihtoEhto = arg.split('!');
                vaihtoEhdot.push(vaihtoEhto[1].toString());
            }
            else if (emojit.includes(arg)) reaktiot.push(arg);
            else kysymys.push(arg);
        }
        const kysymysTeksti = kysymys.join(' ');

        if (vaihtoEhdot.length != reaktiot.length) return message.reply('Vaihtoehtoja ja emojeita oli eri määrä');

        for (i=0;i<vaihtoEhdot.length;i++) {
            const vaihtoEhto = vaihtoEhdot[i];
            const emoji = reaktiot[i];
            valinnat.push(`${emoji} = ${vaihtoEhto}`);
        }

        const embed = new Discord.MessageEmbed()
        .setColor('#e42643')
        .setTitle(kysymysTeksti)
        .setDescription('Valitse vaihtoehdoista sopivin!\n\n' + valinnat.join('\n'));
        let messageEmbed = await message.channel.send({embeds: [embed]});

        for (const emoji of reaktiot) messageEmbed.react(emoji);
        for (i=0;i<vaihtoEhdot.length;i++) {
            vastaukset[vaihtoEhdot[i]] = 0;
        }

        client.on('messageReactionAdd', async (reaction, user) => {

            if (reaction.message.partial) await reaction.message.fetch();
            if (reaction.partial) await reaction.fetch();
            if (user.bot) return;
            if (!reaction.message.guild) return;
            if (reaction.message.channel.id == channel) {
                for (i=0;i<reaktiot.length;i++) {
                    if (reaction.emoji.name == reaktiot[i]) {
                        vastaukset[vaihtoEhdot[i]]++;
                    }
                }
                
            }
            
        });

        client.on('messageReactionRemove', async (reaction, user) => {

            if (reaction.message.partial) await reaction.message.fetch();
            if (reaction.partial) await reaction.fetch();
            if (await user.bot) return;
            if (!reaction.message.guild) return;
            if (reaction.message.channel.id == channel) {
                for (i=0;i<reaktiot.length;i++) {
                    if (reaction.emoji.name == reaktiot[i]) {
                        vastaukset[vaihtoEhdot[i]]--;
                    }
                }
                
            }
            
        });
        
        client.once('messageCreate', async message => {
            if (message.content.toLowerCase() == '-results') {
                var total = 0;
                for (i=0;i<vaihtoEhdot.length;i++) {
                     total += vastaukset[vaihtoEhdot[i]];
                }

                var msg = ``;
                for (i=0;i<vaihtoEhdot.length;i++) {
                    msg += `${reaktiot[i]} ${vaihtoEhdot[i]}: ${(Math.round(vastaukset[vaihtoEhdot[i]]/total*100))}%\n`; 
                }
                const embed = new Discord.MessageEmbed()
                .setColor('#e42643')
                .setTitle(kysymysTeksti + ' - Vastaukset')
                .setDescription('Vastaukset: \n\n' + msg);
                await message.channel.send({embeds: [embed]});
            }
        });
    }

}