const fs = require('fs');

module.exports = {
    name: 'ehota',
    description: 'Saa ehottaa toimintoja',
    async execute(client, message, args, Discord) {
        if (!args[0]) return message.reply('Käytä kirjoittamalla vapaamuotoinen ehotus komennon -ehota perään');
        
        const tekija = message.author.username;
        const teksti = args.join(' ').toString();
        let ehotuksetjson = fs.readFileSync('./ehotukset.json', 'utf-8');
        let ehotukset = JSON.parse(ehotuksetjson);
        const ehotusLkm = ehotukset.length;
        const ehotusArray = {
            'Ehotus': ehotusLkm + 1,
            'Tekijä': tekija,
            'Teksti': teksti
        };
        ehotukset.push(ehotusArray);
        ehotuksetjson = JSON.stringify(ehotukset, null, 4);
        fs.writeFileSync('./ehotukset.json', ehotuksetjson + '\n', 'utf-8');
    }
}