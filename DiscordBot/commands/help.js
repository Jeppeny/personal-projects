const fs = require('fs');

module.exports = {
    name: 'help',
    description: 'Antaa apua',
    async execute(client, message, args, Discord) {
        const commandFiles = fs.readdirSync('./commands/').filter(file => file.endsWith('.js'));
        var commandNames = [];
        for (const file of commandFiles) {
            const command = require(`../commands/${file}`);
            if (command.name) {
                commandNames.push(command.name);
            }
            else {
                continue;
            }
        }
        komennot = commandNames.join('\n-');
        message.reply('Saatavilla olevat komennot:\n-' + komennot);
    }
}