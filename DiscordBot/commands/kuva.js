var Scraper = require('images-scraper');

const google = new Scraper({
    puppeteer: {
        headless: true
    }
});

module.exports = {
    name: 'kuva',
    description:'Etsii kuvan googlesta',
    async execute(client, message, args) {
        if (!args[0]) return message.reply('Käyttö: -kuva (-random) hakutermit');
        if (args[0].toLowerCase() == '-random') {
            if (!args[1]) return message.reply('Käyttö: -kuva (-random) hakutermit');
            try {
                args.shift();
                const haku = args.join(' ');
                const imageResults = await google.scrape(haku, 500);
                let kuvaNro = Math.floor(Math.random() * imageResults.length);
                return message.channel.send(imageResults[kuvaNro].url);
            } catch (err) {
                console.log(err);
                return message.channel.send('Kuvia ei löytynyt / haku epäonnistui');
            }
        }
        else {
            const haku = args.join(' ');
            try {
                const imageResults = await google.scrape(haku, 1);
                return message.channel.send(imageResults[0].url);
            } catch (err) {
                console.log(err);
                return message.channel.send('Kuvia ei löytynyt / haku epäonnistui');
            }
        }
    }
}