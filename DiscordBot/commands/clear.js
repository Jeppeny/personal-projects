const bot_config = require('../bot_config.js');

module.exports = {
    name: 'clear',
    description: 'Clears messages',
    async execute(client, message, args, Discord) {
        if (!args[0]) return message.reply('Käytä kirjoittamalla poistettavien viestien lukumäärä komennon -clear perään');
        if (isNaN(args[0])) return message.reply('Anna numero!');
        if (args[0] > 20 && message.author.id != bot_config.server_owner) return message.reply('Et voi poistaa yli 20 viestiä kerralla!');
        if (args[0] >= 100) return message.reply('KairaPro ei voi poistaa yli 100 viestiä kerralla t discord api');
        if (args[0] < 1) return message.reply('Kerralla on poistettava vähintään yksi viesti!');

        args[0]++;
        await message.channel.messages.fetch({limit: args[0]})
        .then(messages => {
            message.channel.bulkDelete(messages);
        })
    }
}