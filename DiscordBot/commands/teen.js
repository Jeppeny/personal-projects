module.exports = {
    name: 'teen',
    description: 'Vastaa olen ___',
    execute(client, message, args) {
        var msg = '';
        if (args[1]) {
            msg = args.join(' ');
        }
        else {
            msg = args[0];
        }
        message.reply({
            content: 'OLEN ' + msg.toUpperCase(),
        });
    }
}