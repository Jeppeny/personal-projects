const { MessageManager, ReactionCollector } = require("discord.js");

module.exports = {
    name: 'gamble',
    description: 'gambling',
    async execute (client, message, args, Discord) {
        const channel = message.channel.id;
        const pelaaja1 = message.author.username;
        const pelit = ['ruletti'];
        await message.reply(`Mitä peliä haluat pelata\nSaatavilla: ${pelit.join(', ')}`);

        client.once('messageCreate', message => {
            if (message.author.bot) return;
            if (message.author.username != pelaaja1) return;
            if (!message.guild) return;
            if (message.channel.id == channel) {
                if (message.content == '-ruletti') return ruletti(pelaaja1);
            }
        });

        const ruletti = async (pelaaja1) => {
            console.log('sus');
            var players = {
                'player1': {
                    'name': pelaaja1,
                    'points': 0
                },
            };
            console.log(players);
            await message.reply('Pelaaja 2, lähetä viesti -join');
            client.once('messageCreate', message => {
                if (message.channel.id != channel) return;
                if (message.content.toLowerCase() == '-join') {
                    players['player2'] = {
                        'name': message.author.username,
                        'points': 0
                    };
                    console.log(players);
                    message.reply(`Players:\n${players['player1']['name']} - ${players['player1']['points']}\n${players['player2']['name']} - ${players['player2']['points']}`);
                }
            });
        }

    }
}