const fs = require('fs');

module.exports = {
    name: 'ehotukset',
    description: 'Näytä ehotukset',
    async execute(client, message, args, Discord) {
        if (args[0]) return message.reply('-ehotukset ei ota argumentteja!');

        let ehotuksetjson = fs.readFileSync('./ehotukset.json', 'utf-8');
        let ehotukset = JSON.parse(ehotuksetjson);
        let tekstit = [];

        if (ehotukset.length == 0) return message.reply('Ei ehotuksia! Käytä -ehota');

        for (ehotus of ehotukset) {
            const t = ehotus['Teksti'];
            tekstit.push(t);
        }
        const msg = tekstit.join('\n\n');

        try {
            message.reply(msg);
        } catch (err) {
            message.reply('Error');
            console.log(err);
        }
    }
}