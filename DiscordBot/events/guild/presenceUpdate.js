const bot_config = require('../../bot_config.js');

module.exports = async (client, Discord, oldPresence, newPresence) => {
    if (oldPresence == undefined || newPresence == undefined) {
        return;
    }
    const oldActivity = oldPresence.activities.find(activity => activity.timestamps != null);
    if (oldActivity) {
        const newActivity = newPresence.activities.find(activity => activity.name == oldActivity.name);
        if (oldActivity.name == '@everyone' || oldActivity.name == '@here') {
            return;
        }
        if (newActivity == undefined && (oldActivity.name.toLowerCase() == 'league of legends' || oldActivity.name.toLowerCase() == 'fortnite')) {
            const a = new Date();
            const b = oldActivity.timestamps.start;
            if (b <= 0) {
                return;
            }
            const m = Math.abs(a - b) / 36e5 * 60;
            if (m >= 30) {
                const names = ['Touch grass', 'Go outside', `I played ${oldActivity.name}`, `I'm a ${oldActivity.name} player`, 'I should touch grass', 'I should go outside'];
                const name = names[Math.floor(Math.random() * names.length)];
                const userID = oldPresence.user.id;
                const guild = await client.guilds.cache.get(bot_config.server_ID);
                const member = await guild.members.cache.get(userID);
                member.setNickname(name);
                console.log(`${oldPresence.user.username} has been playing ${oldActivity.name} for ${Math.round(m)} minutes.`);
                return client.channels.cache.get(bot_config.bot_channel).send(`<@${oldPresence.user.id}> has been playing ${oldActivity.name} for ${Math.round(m)} minutes, please touch grass!`);
            }
        }
    }
}