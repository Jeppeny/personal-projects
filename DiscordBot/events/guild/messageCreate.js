module.exports = async (client, Discord, message) => {
    const prefix = '-';
    const kielletytSanat = ['neekeri', 'nigger', 'faggot'];
    for (const sana of kielletytSanat) {
        if (message.content.toLowerCase().includes(sana)) {
            await message.delete();
            let vuosi = Math.floor(Math.random() * 100);
            var vuosiLuku;
            if (vuosi < 10) {
                const array1 = ['0', vuosi.toString()];
                vuosiLuku = array1.join('');
            }
            else {
                vuosiLuku = vuosi.toString();
            }
            return await message.channel.send(`Literally George Orwin 19${vuosiLuku} Animal Crossings`);
        }
    }

    // if (message.content.toLowerCase() == 'teen kamaa') {
    //     const cmd = client.commands.get('sus');
    //     return cmd.execute(client, message, null, Discord);
    // }

    if (!message.content.startsWith(prefix) || message.author.bot) {
        if (message.content.toLowerCase().startsWith('teen')) {
            const args = message.content
            .split(/ +/);
            const cmd = args
            .shift()
            .toLowerCase();
            client.commands.get(cmd).execute(client, message, args);
        }
        return;
    }

    const args = message.content.slice(prefix.length).split(/ +/);
    const cmdName = args.shift().toLowerCase();
    const command = client.commands.get(cmdName);

    if (command) {
        command.execute(client, message, args, Discord);
    }
    
}