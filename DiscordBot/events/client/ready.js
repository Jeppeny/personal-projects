const bot_config = require('../../bot_config.js');

module.exports = (client) => {
    console.log('\nAnoBotti is online!\n');
    client.channels.cache.get(bot_config.bot_channel).send('AnoBotti is now online!');
}